import 'package:get/get.dart';
import '../controllers/addtask_controller.dart';
import '../controllers/forgot_password_controller.dart';
import '../controllers/login_controller.dart';
import '../controllers/new_task_list_data_controller.dart';
import '../controllers/otp_controller.dart';
import '../controllers/overview_controller.dart';
import '../controllers/rest_pass_controller.dart';
import '../controllers/signup_controller.dart';
import '../controllers/splash_screen_sontroller.dart';
import '../controllers/task_cancelled_controller.dart';
import '../controllers/task_completed_controller.dart';
import '../controllers/task_progress_controller.dart';
import '../controllers/update_profile_controller.dart';


class AppBindings extends Bindings {
  @override
  void dependencies() {
    Get.put<LoginController>(LoginController());
    Get.put<SignUpController>(SignUpController());
    Get.put<ForgetPassController>(ForgetPassController());
    Get.put<OtpController>(OtpController());
    Get.put<ResetPassController>(ResetPassController());
    Get.put<UpdateProfileController>(UpdateProfileController());
    Get.put<AddTaskController>(AddTaskController());
    Get.put<SplashController>(SplashController());
    Get.put<OverviewController>(OverviewController());
    Get.put<GetNewTaskListDataController>(GetNewTaskListDataController());
    Get.put<TaskCancelledController>(TaskCancelledController());
    Get.put<TaskCompletedController>(TaskCompletedController());
    Get.put<TaskProgressController>(TaskProgressController());
  }
}
