import 'package:get/get.dart';
import '../../api_data/api_services/network_service_handler.dart';
import '../../api_data/models/network_services_response.dart';
import '../../api_data/utils/data_utils.dart';

class AddTaskController extends GetxController {
  bool _isLoading = false;

  bool get isLoading => _isLoading;

  Future<bool> addNewTaskMethod(String title, String description) async {
    _isLoading = true;
    update();

    Map<String, dynamic> responseBody = {
      "title": title,
      "description": description,
      "status": 'New',
    };

    final NetworkResponse response = await NetworkServiceHandler()
        .postRequest(Urls.createNewTaskUrls, responseBody);

    _isLoading = false;
    update();

    if (response.isSuccess) {
      return true;
    } else {
      return false;
    }
  }
}
