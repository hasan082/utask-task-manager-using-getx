import 'package:get/get.dart';
import '../../api_data/api_services/network_service_handler.dart';
import '../../api_data/models/network_services_response.dart';
import '../../api_data/models/task_overview_model.dart';
import '../../api_data/utils/data_utils.dart';

class OverviewController extends GetxController {

  bool _isTaskOverViewPageProgress = false;

  TaskOverviewModel _taskOverviewModel = TaskOverviewModel();

  bool get isTaskOverViewPageProgress => _isTaskOverViewPageProgress;
  TaskOverviewModel get taskOverviewModel => _taskOverviewModel;


  Future<bool> getTaskOverviewData() async {
    _isTaskOverViewPageProgress = true;
    update();

    final NetworkResponse response =
    await NetworkServiceHandler().getRequest(Urls.taskOverUrl);


      if (response.isSuccess) {
        _taskOverviewModel = TaskOverviewModel.fromJson(response.body!);
        _isTaskOverViewPageProgress = false;

        update();
        return true;
      }else {
        update();
        return false;
        // if (mounted) {
        //   ScaffoldMessenger.of(context).showSnackBar(
        //       const SnackBar(content: Text('Overview Data loading failed')));
        //   isTaskOverViewPageProgress = false;
        //   update();
        }
      }



  }
