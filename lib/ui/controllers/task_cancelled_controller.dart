import 'package:get/get.dart';
import '../../api_data/api_services/network_service_handler.dart';
import '../../api_data/models/network_services_response.dart';
import '../../api_data/models/task_list_model.dart';
import '../../api_data/utils/data_utils.dart';

class TaskCancelledController extends GetxController {

  late bool _isLoading = false;
  TaskListModel _taskListModel = TaskListModel();

  bool get isLoading => _isLoading;
  TaskListModel get taskListModel => _taskListModel;

  Future<bool> getCancelledTaskListData() async {
    _isLoading = true;
    update();

    final NetworkResponse response =
    await NetworkServiceHandler().getRequest(Urls.cancelledTaskDataUrl);

    _isLoading = false;
    update();

    if (response.isSuccess) {

      _taskListModel = TaskListModel.fromJson(response.body!);

      update();
      return true;

    } else {
      return false;

      // if (mounted) {
      //   ScaffoldMessenger.of(context).showSnackBar(
      //       const SnackBar(content: Text('Cancelled data loading failed')));
      //   isLoading = false;
      //   if (mounted) {
      //     setState(() {});
      //   }
      // }
    }
  }

}