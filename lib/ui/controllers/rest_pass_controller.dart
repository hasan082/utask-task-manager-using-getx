import 'package:get/get.dart';
import '../../api_data/api_services/network_service_handler.dart';
import '../../api_data/models/network_services_response.dart';
import '../../api_data/utils/data_utils.dart';

class ResetPassController extends GetxController {

  bool obscureState = true,
      _isLoading = false,
      confirmObscureState = true,
      passwordMatch = true;

  void toggleObscureState() {
    obscureState = !obscureState;
    update();
  }

  void toggleConfirmObscureState() {
    confirmObscureState = !confirmObscureState;
    update();
  }

  bool get isLoading => _isLoading;



  void checkPasswordMatch(String pass, String confirmPass) {
    final String password = pass;
    final String confirmPassword = confirmPass;
    passwordMatch = password == confirmPassword;
    update();
  }

  Future<bool> resetPasswordMethod(String email, String otp, String password) async {
    _isLoading = true;
    update();


    Map<String, dynamic> requestBody = {
      "email": email,
      "OTP": otp,
      "password": password
    };

    final NetworkResponse response =
    await NetworkServiceHandler().postRequest(Urls.resetPasswordUrl, requestBody);


    _isLoading = false;
    update();

    if (response.isSuccess) {
      return true;
        // Get.offAllNamed('/login');
        // ScaffoldMessenger.of(context).showSnackBar(
        //     const SnackBar(content: Text('Password reset Successfully')));

    } else {
      return false;
        // ScaffoldMessenger.of(context).showSnackBar(
        //     const SnackBar(content: Text('Password reset failed')));
    }
  }

}