import 'package:get/get.dart';
import '../../api_data/api_services/network_service_handler.dart';
import '../../api_data/utils/data_utils.dart';

class SignUpController extends GetxController {

  bool _isLoading = false;
  bool get isLoading => _isLoading;

  bool obscureState = true;
  void toggleObscureState() {
    obscureState = !obscureState;
    update();
  }



  Future<bool> userSignUp(String email, String firstName, String lastName, String mobile, String password) async {
    _isLoading = true;
    update();

    NetworkServiceHandler networkServiceHandler = NetworkServiceHandler();

    Map<String, dynamic> requestBody = {
      "email": email,
      "firstName": firstName,
      "lastName": lastName,
      "mobile": mobile,
      "password": password,
      "photo": "",
    };

    final response = await networkServiceHandler.postRequest(
      Urls.registrationUrls,
      requestBody,
    );


    _isLoading = false;
    update();

    if (response.isSuccess) {
      return true;

    } else {
      return false;

    }


  }


  Future<bool> checkEmailExists(String email) async {
    _isLoading = true;
    update();
      NetworkServiceHandler networkServiceHandler = NetworkServiceHandler();

      Map<String, dynamic> requestBody = {
        "email": email,
      };
      final response = await networkServiceHandler.postRequest(
        Urls.loginUrls,
        requestBody,
      );
      _isLoading = false;
      update();
      if (response.statusCode == 200) {
        return true;
      } else {
        return false;
      }
  }




}