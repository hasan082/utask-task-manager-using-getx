import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';

import '../../api_data/api_services/network_service_handler.dart';
import '../../api_data/models/network_services_response.dart';
import '../../api_data/utils/authentication_utils.dart';
import '../../api_data/utils/data_utils.dart';

class UpdateProfileController extends GetxController {

  bool obscureState = true;
  bool isLoading = false;

  XFile? imageFile;

  final ImagePicker _picker = ImagePicker();


  void toggleObscureState() {
    obscureState = !obscureState;
    update();
  }

  void selectImage() {
    _picker.pickImage(source: ImageSource.gallery).then((xFile) {
      if (xFile != null) {
        imageFile = xFile;
        update();
      }
    });
  }



  Future<bool> updateUserProfile(String firstName, String lastName, String mobile, String pass) async {

    isLoading = true;
    update();

    Map<String, dynamic> requestBody = {
      "email": AuthUtility.userInfo.data!.email,
      "firstName": firstName,
      "lastName": lastName,
      "mobile": mobile,
    };

    if (pass.isNotEmpty) {
      requestBody["password"] = pass;
    }

    if (imageFile != null) {
      requestBody["photo"] = imageFile!.path;
    } else {
      requestBody["photo"] = AuthUtility.userInfo.data!.photo;
    }

    final NetworkResponse response = await NetworkServiceHandler()
        .postRequest(Urls.profileUpdateUrls, requestBody);

    isLoading = false;
    update();

    if (response.isSuccess) {
      AuthUtility.userInfo.data!.firstName = requestBody["firstName"];
      AuthUtility.userInfo.data!.lastName = requestBody["lastName"];
      AuthUtility.userInfo.data!.mobile = requestBody["mobile"];
      AuthUtility.userInfo.data!.photo = requestBody["photo"];

      await AuthUtility.saveUserInfo(AuthUtility.userInfo);

      return true;
    } else {
      return false;
    }
  }



}