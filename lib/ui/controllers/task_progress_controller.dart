import 'package:get/get.dart';
import '../../api_data/api_services/network_service_handler.dart';
import '../../api_data/models/network_services_response.dart';
import '../../api_data/models/task_list_model.dart';
import '../../api_data/utils/data_utils.dart';

class TaskProgressController extends GetxController {

  late bool _isLoading = false;
  TaskListModel _taskListModel = TaskListModel();

  bool get isLoading => _isLoading;
  TaskListModel get taskListModel => _taskListModel;


  Future<bool> getProgressTaskListData() async {
    _isLoading = true;
    update();
    final NetworkResponse response =
    await NetworkServiceHandler().getRequest(Urls.progressTaskDataUrl);
    _isLoading = false;
    if (response.isSuccess) {
      _taskListModel = TaskListModel.fromJson(response.body!);
      update();
      return true;
    } else {
      update();
      return false;
    }
  }

}