import 'package:get/get.dart';
import '../../api_data/api_services/network_service_handler.dart';
import '../../api_data/models/network_services_response.dart';
import '../../api_data/models/task_list_model.dart';
import '../../api_data/utils/data_utils.dart';


class GetNewTaskListDataController extends GetxController {

  late bool _isNewTaskListInProgress = false;

  TaskListModel _taskListModel = TaskListModel();


  TaskListModel get taskListModel => _taskListModel;

  bool get isNewTaskListInProgress => _isNewTaskListInProgress;

  Future<bool> getNewTaskListData() async {

    _isNewTaskListInProgress = true;
    update();
    final NetworkResponse response =
    await NetworkServiceHandler().getRequest(Urls.newTaskDataUrl);

    _isNewTaskListInProgress = false;
    update();

    if (response.isSuccess) {

      _taskListModel = TaskListModel.fromJson(response.body!);

      update();

      return true;
    } else {
      return false;
      // if (mounted) {
      //   ScaffoldMessenger.of(context).showSnackBar(
      //       const SnackBar(content: Text('New Data loading failed')));
      // }
    }
  }

}

