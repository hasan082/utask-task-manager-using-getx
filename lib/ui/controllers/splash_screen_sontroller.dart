import 'dart:async';
import 'package:flutter/animation.dart';
import 'package:get/get.dart';
import '../../api_data/utils/authentication_utils.dart';

class SplashController extends GetxController with GetSingleTickerProviderStateMixin {
  late AnimationController _controller;

  AnimationController get controller => _controller;

  Future<bool> animationControllerMethod() async {
    _controller = AnimationController(
      vsync: this,
      duration: const Duration(seconds: 2),
    );

    Completer<bool> completer = Completer<bool>();

    _controller.forward().whenComplete(() async {
      bool isLoggedIn = await AuthUtility.checkUserLoggedIn();
      completer.complete(isLoggedIn);
    });

    return completer.future; // Return the Future
  }
}

