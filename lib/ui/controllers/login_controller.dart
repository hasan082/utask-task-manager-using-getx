import 'dart:developer';

import 'package:get/get.dart';
import '../../api_data/api_services/network_service_handler.dart';
import '../../api_data/models/login_response_model.dart';
import '../../api_data/models/network_services_response.dart';
import '../../api_data/utils/authentication_utils.dart';
import '../../api_data/utils/data_utils.dart';

class LoginController extends GetxController {
  bool _isLoading = false;

  bool obscureState = true;

  void toggleObscureState() {
    obscureState = !obscureState;
    update();
  }

  bool get isLoading => _isLoading;

  Future<bool> userLogIn(String email, String password) async {
    _isLoading = true;
    update();

    Map<String, dynamic> responseBody = {
      "email": email,
      "password": password,
    };

    final NetworkResponse response = await NetworkServiceHandler()
        .postRequest(Urls.loginUrls, responseBody, isLogin: true);


    _isLoading = false;
    update();

    if (response.isSuccess && response.body?['status'] == 'success') {

      await AuthUtility.saveUserInfo(LoginResponseModel.fromJson(response.body!));

      update();

      return true;
    } else {
      update();
      return false;
    }
  }
}
