import 'package:get/get.dart';
import '../../api_data/api_services/network_service_handler.dart';
import '../../api_data/models/network_services_response.dart';
import '../../api_data/models/task_list_model.dart';
import '../../api_data/utils/data_utils.dart';

class TaskCompletedController extends GetxController {

  late bool _isLoading = false;
  TaskListModel _taskListModel = TaskListModel();

  bool get isLoading => _isLoading;
  TaskListModel get taskListModel => _taskListModel;

  Future<bool> getCompletedTaskListData() async {
    _isLoading = true;
    update();

    final NetworkResponse response =
    await NetworkServiceHandler().getRequest(Urls.completedTaskDataUrl);

    _isLoading = false;

    if (response.isSuccess) {
      _taskListModel = TaskListModel.fromJson(response.body!);
      update();
      return true;
    } else {
      return false;
    }
  }
}