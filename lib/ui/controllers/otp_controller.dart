import 'package:get/get.dart';

import '../../api_data/api_services/network_service_handler.dart';
import '../../api_data/models/network_services_response.dart';
import '../../api_data/utils/data_utils.dart';

class OtpController extends GetxController {
  bool _isLoading = false;

  bool get isLoading => _isLoading;

  Future<bool> otpVerifyMethod(String email, String otp) async {
    _isLoading = true;
    update();

    final NetworkResponse response =
        await NetworkServiceHandler().getRequest(Urls.otpVerifyUrl(email, otp));

    _isLoading = false;
    update();

    if (response.isSuccess && response.body?['status'] == 'success') {
      return true;
    } else {
      return false;
    }
  }
}
