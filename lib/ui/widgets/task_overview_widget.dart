import 'package:flutter/material.dart';

class TaskOverview extends StatelessWidget {
  const TaskOverview({
    super.key,
    required this.taskNumber,
    required this.overviewStatus,
  });

  final int taskNumber;
  final String overviewStatus;

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Card(
        elevation: 3,
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 10.0, horizontal: 3.0),
          child: Column(
            children: [
              Text(
                taskNumber.toString(),
                style: Theme.of(context).textTheme.titleSmall,
              ),
              const SizedBox(
                height: 4,
              ),
              FittedBox(
                  child: Text(
                overviewStatus,
                style: Theme.of(context).textTheme.bodySmall,
              )),
            ],
          ),
        ),
      ),
    );
  }
}
