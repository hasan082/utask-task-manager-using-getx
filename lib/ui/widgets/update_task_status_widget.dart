import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:itask_complete_project/api_data/models/task_list_model.dart';
import '../../api_data/api_services/network_service_handler.dart';
import '../../api_data/models/network_services_response.dart';
import '../../api_data/utils/data_utils.dart';

class UpdateTaskStatus extends StatefulWidget {
  final TaskData task;
  final VoidCallback onUpdate;
  const UpdateTaskStatus({Key? key, required this.task, required this.onUpdate}) : super(key: key);

  @override
  State<UpdateTaskStatus> createState() => _UpdateTaskStatusState();
}

class _UpdateTaskStatusState extends State<UpdateTaskStatus> {

  List<String> taskStatusList = ['New', 'Progress', 'Completed', 'Cancelled'];
  late String _selectedTask;
  bool updateTaskInProgress = false;

@override
  void initState() {
  _selectedTask = widget.task.status!;
    super.initState();
  }



  Future<void> updateTaskStatus(String taskId, String newStatus) async {

    updateTaskInProgress = true;
    if (mounted) {
      setState(() {});
    }

    final NetworkResponse response = await NetworkServiceHandler()
        .getRequest(Urls.updateTaskStatusUrl(taskId, newStatus));

    updateTaskInProgress = false;
    if (mounted) {
      setState(() {});
    }

    if (response.isSuccess) {
      widget.onUpdate();
      if (mounted) {
        setState(() {});
        Get.back();
        ScaffoldMessenger.of(context).showSnackBar(
            const SnackBar(content: Text('Task Status Updated')));
      }
    } else {
      if (mounted) {
        ScaffoldMessenger.of(context)
            .showSnackBar(const SnackBar(content: Text('Status Update failed')));
      }
    }
  }



  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 16.0),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          ListView.builder(
              padding: const EdgeInsets.symmetric(vertical: 10),
              shrinkWrap: true,
              itemCount: taskStatusList.length,
              itemBuilder: (context, index) {
                return ListTile(
                  onTap: () {
                    _selectedTask = taskStatusList[index];
                    setState(() {

                    });
                  },
                  leading: _selectedTask == taskStatusList[index]
                      ? Icon(Icons.check_box, color: Theme.of(context).primaryColor)
                      : const Icon(Icons.square_outlined),
                  title: Text(taskStatusList[index]),
                );
              }),
          Visibility(
            visible: !updateTaskInProgress,
            replacement: const Center(child: CircularProgressIndicator(),),
            child: ElevatedButton(onPressed: (){
              updateTaskStatus(widget.task.sId!, _selectedTask);
            }, child: const Text('Update'),
            ),
          )
        ],
      ),
    );
  }
}
