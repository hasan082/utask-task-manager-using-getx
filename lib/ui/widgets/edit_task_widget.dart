import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../api_data/api_services/network_service_handler.dart';
import '../../api_data/models/network_services_response.dart';
import '../../api_data/models/task_list_model.dart';
import '../../api_data/utils/data_utils.dart';

class EditTaskBottomSheet extends StatefulWidget {
  final TaskData task;
  final VoidCallback onUpdateStatus;

  const EditTaskBottomSheet({super.key, required this.task, required this.onUpdateStatus});

  @override
  State<EditTaskBottomSheet> createState() => _EditTaskBottomSheetState();
}

class _EditTaskBottomSheetState extends State<EditTaskBottomSheet> {
  late TextEditingController _titleCtrl;

  late TextEditingController _descriptionCtrl;
  bool isUpdateTaskInProgress = false;

  @override
  void initState() {
    super.initState();
    _titleCtrl = TextEditingController(text: widget.task.title);
    _descriptionCtrl = TextEditingController(text: widget.task.description);
  }



  Future<void> updateTaskStatusMethod() async {
    isUpdateTaskInProgress = true;
    if (mounted) {
      setState(() {});
    }

    Map<String, dynamic> responseBody = {
      "title": _titleCtrl.text.trim(),
      "description": _descriptionCtrl.text.trim(),
    };

    final NetworkResponse response = await NetworkServiceHandler()
        .postRequest(Urls.createNewTaskUrls, responseBody);

    if (response.isSuccess) {
      isUpdateTaskInProgress = false;
      if (mounted) {
        setState(() {});
      }
      _titleCtrl.clear();
      _descriptionCtrl.clear();
      if (mounted) {
        ScaffoldMessenger.of(context).showSnackBar(
            const SnackBar(content: Text('Updated Added Successfully')));
        widget.onUpdateStatus();
      } else {
        ScaffoldMessenger.of(context)
            .showSnackBar(const SnackBar(content: Text('Update Failed')));
      }
    } else {
      isUpdateTaskInProgress = false;
      if (mounted) {
        ScaffoldMessenger.of(context)
            .showSnackBar(const SnackBar(content: Text('Update API Failed')));
        setState(() {});
      }
    }
  }







  @override
  Widget build(BuildContext context) {
    return Padding(
      padding:
      EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom),
      child: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(25.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                children: [
                  Text(
                    'Update Task',
                    style: Theme.of(context).textTheme.titleLarge,
                  ),
                  const Spacer(),
                  IconButton(
                    onPressed: () {
                      Get.back();
                    },
                    icon: const Icon(Icons.close),
                  ),
                ],
              ),
              const SizedBox(height: 15),
              TextFormField(
                controller: _titleCtrl,
                decoration: const InputDecoration(hintText: 'Title'),
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'Please enter a title.';
                  }
                  return null; // Return null if the value is valid
                },
              ),
              const SizedBox(height: 15),
              TextFormField(
                controller: _descriptionCtrl,
                maxLines: 5,
                decoration: const InputDecoration(
                  hintText: 'Description',
                ),
              ),
              const SizedBox(height: 15),
              SizedBox(
                width: double.infinity,
                child: Visibility(
                  visible: !isUpdateTaskInProgress,
                  replacement: const Center(child: CircularProgressIndicator()),
                  child: ElevatedButton(
                    onPressed: () {
                      updateTaskStatusMethod();
                    },
                    child: const Text('Update'),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
