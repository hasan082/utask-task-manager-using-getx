import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import '../../utils/assets_utils.dart';

class ScreenBg extends StatelessWidget {
  final Widget child;
  const ScreenBg({Key? key, required this.child}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Stack(
        children: [
          SizedBox(
            width: double.infinity,
            height: double.infinity,
            child: SvgPicture.asset(AssetUtils.bgSvg,fit: BoxFit.cover,),
          ),
          child
        ],
      ),
    );
  }
}