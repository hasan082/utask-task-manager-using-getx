import 'package:flutter/material.dart';


enum SlideDirection { leftToRight, rightToLeft }

class CustomPageRoute extends PageRouteBuilder {
  final Widget page;
  final RouteSettings? pageSettings;
  final SlideDirection axis;
  final String? routeName;

  CustomPageRoute({
    required this.page,
    this.pageSettings,
    this.axis = SlideDirection.leftToRight,
    this.routeName,
  }) : super(
    settings: pageSettings,
    pageBuilder: (context, animation, secondaryAnimation) => page,
    transitionsBuilder: (context, animation, secondaryAnimation, child) {
      Offset begin;
      Offset end;

      if (axis == SlideDirection.leftToRight) {
        begin = const Offset(1.0, 0.0);
        end = Offset.zero;
      } else {
        begin = const Offset(-1.0, 0.0);
        end = Offset.zero;
      }

      const curve = Curves.ease;

      var tween = Tween(begin: begin, end: end).chain(CurveTween(curve: curve));
      var offsetAnimation = animation.drive(tween);

      return SlideTransition(
        position: offsetAnimation,
        child: child,
      );
    },
  );
}

