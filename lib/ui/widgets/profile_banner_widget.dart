import 'dart:io';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:itask_complete_project/api_data/utils/authentication_utils.dart';

class ProfileBannerWidget extends StatefulWidget
    implements PreferredSizeWidget {
  final bool? isuUpdateScreen;

  const ProfileBannerWidget({Key? key,this.isuUpdateScreen})
      : super(key: key);

  @override
  State<ProfileBannerWidget> createState() => _ProfileBannerWidgetState();

  @override
  Size get preferredSize => const Size.fromHeight(kToolbarHeight);
}

class _ProfileBannerWidgetState extends State<ProfileBannerWidget> {



  Widget buildProfileAvatar() {
    final String? photoUrl = AuthUtility.userInfo.data!.photo;
    if (photoUrl != null && photoUrl.isNotEmpty) {
      return CircleAvatar(
        backgroundImage: FileImage(File(photoUrl)),
      );
    } else {
      return const CircleAvatar(
        child: Icon(Icons.person),
      );
    }
  }



  @override
  Widget build(BuildContext context) {
    return AppBar(
      title: InkWell(
        onTap: () {
          if (widget.isuUpdateScreen != true) {
            Get.toNamed('/updateProfile');
          }
        },
        child: Column(
          children: [
            Row(
              children: [
                Visibility(
                  visible: widget.isuUpdateScreen != true,
                  child: buildProfileAvatar(),
                ),
                const SizedBox(
                  width: 16,
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      '${AuthUtility.userInfo.data?.firstName ?? "Your"} ${AuthUtility.userInfo.data?.lastName ?? "Name"}',
                      style: const TextStyle(fontSize: 16, color: Colors.white),
                    ),
                    const SizedBox(
                      width: 16,
                    ),
                    Text(
                      AuthUtility.userInfo.data?.email ?? "your email",
                      style: const TextStyle(fontSize: 14, color: Colors.white),
                    ),
                  ],
                ),
              ],
            ),
          ],
        ),
      ),
      actions: [
        IconButton(
          onPressed: () async {
            await AuthUtility.clearUserInfo();
            Get.offAllNamed('/login');
          },
          icon: const Icon(
            Icons.logout_sharp,
            color: Colors.white,
          ),
        ),
      ],
    );
  }
}
