import 'package:flutter/material.dart';
import '../../api_data/models/task_list_model.dart';

class TaskSingleItemWidget extends StatefulWidget {
  final Color chipBgColor;
  final TaskData taskData;
  final VoidCallback onTapDelete, onTapStatusChange;
  const TaskSingleItemWidget({
    super.key, required this.chipBgColor, required this.taskData, required this.onTapDelete, required this.onTapStatusChange,
  });


  @override
  State<TaskSingleItemWidget> createState() => _TaskSingleItemWidgetState();
}

class _TaskSingleItemWidgetState extends State<TaskSingleItemWidget> {

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 3,
      child: Padding(
        padding: const EdgeInsets.only(top: 8.0),
        child: ListTile(
          isThreeLine: true,
          title: Text(
            widget.taskData.title ?? 'Unknown',
            style: Theme.of(context).textTheme.titleSmall,
          ),
          subtitle: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const SizedBox(
                height: 7,
              ),
              Text(
                widget.taskData.description ?? 'Unknown',
              ),
              const SizedBox(
                height: 8,
              ),
              Text( widget.taskData.createdDate ?? '',),
              const SizedBox(
                height: 5,
              ),
              Row(
                children: [
                  Chip(
                    label: Text(
                      widget.taskData.status ?? 'Unknown',
                      style: const TextStyle(fontSize: 13, color: Colors.white),
                    ),
                    backgroundColor: widget.chipBgColor,
                  ),
                  const Spacer(),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      InkWell(
                        onTap: widget.onTapStatusChange,
                        child: const Icon(
                          Icons.edit_note_sharp,
                          color: Colors.green,
                        ),
                      ),
                      const SizedBox(
                        width: 10,
                      ),
                      InkWell(
                          onTap: widget.onTapDelete,
                          child: const Icon(
                            Icons.delete_forever_sharp,
                            color: Colors.red,
                          ))
                    ],
                  )
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
