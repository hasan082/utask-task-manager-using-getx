import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:itask_complete_project/api_data/utils/data_utils.dart';
import '../../api_data/api_services/network_service_handler.dart';
import '../../api_data/models/network_services_response.dart';
import '../../api_data/models/task_list_model.dart';
import '../controllers/task_completed_controller.dart';
import '../widgets/task_single_item_widget.dart';
import '../widgets/update_task_status_widget.dart';

class TaskCompleted extends StatefulWidget {
  const TaskCompleted({Key? key}) : super(key: key);

  @override
  State<TaskCompleted> createState() => _TaskCompletedState();
}

class _TaskCompletedState extends State<TaskCompleted> {

  TaskCompletedController taskCompletedController = Get.find<TaskCompletedController>();

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) {
      taskCompletedController.getCompletedTaskListData();
    });
  }



  Future<void> deleteTask(String taskId) async {
    final NetworkResponse response = await NetworkServiceHandler()
        .getRequest(Urls.deleteTaskStatusUrl(taskId));
    if (response.isSuccess) {
      taskCompletedController.taskListModel.data!.removeWhere((element) => element.sId == taskId);
      if (mounted) {
        setState(() {});
        ScaffoldMessenger.of(context).showSnackBar(
            const SnackBar(content: Text('Task deleted successfully')));
      }
    } else {
      if (mounted) {
        ScaffoldMessenger.of(context)
            .showSnackBar(const SnackBar(content: Text('Deletion failed')));
      }
    }
  }



  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Column(
          children: [
            Expanded(
              child: GetBuilder<TaskCompletedController>(builder: (_){
                if(taskCompletedController.isLoading){
                  return const Center(child: CircularProgressIndicator(),);
                }else {
                  return Padding(
                    padding:
                    const EdgeInsets.symmetric(vertical: 10.0, horizontal: 6),
                    child: RefreshIndicator(
                      onRefresh: () async {
                        taskCompletedController.getCompletedTaskListData();
                      },
                      child: ListView.separated(
                        shrinkWrap: true,
                        physics: const BouncingScrollPhysics(),
                        itemCount: taskCompletedController.taskListModel.data?.length ?? 0,
                        itemBuilder: (context, index) {
                          return TaskSingleItemWidget(
                            chipBgColor: const Color(0xFF3BBF72),
                            taskData: taskCompletedController.taskListModel.data![index],
                            onTapDelete: () {
                              deleteTask(taskCompletedController.taskListModel.data![index].sId!);
                            },
                            onTapStatusChange: () {
                              statusUpdateBottomSheet(
                                  taskCompletedController.taskListModel.data![index]);
                            },
                          );
                        },
                        separatorBuilder: (BuildContext context, int index) {
                          return const SizedBox(
                            height: 10,
                          );
                        },
                      ),
                    )
                  );
                }
              },)
            )
          ],
        ),
      ),
    );
  }

  void statusUpdateBottomSheet(TaskData task) {
    showModalBottomSheet(
        isScrollControlled: true,
        context: context,
        builder: (context) {
          return StatefulBuilder(
            builder: (context, updateState) {
              return UpdateTaskStatus(
                  task: task,
                  onUpdate: () {
                    taskCompletedController.getCompletedTaskListData();
                  });
            },
          );
        });
  }


}
