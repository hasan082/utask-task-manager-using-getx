import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:itask_complete_project/ui/widgets/profile_banner_widget.dart';
import 'package:itask_complete_project/ui/widgets/screen_bg.dart';
import '../../../api_data/api_services/network_service_handler.dart';
import '../../../api_data/models/network_services_response.dart';
import '../../../api_data/utils/authentication_utils.dart';
import '../../../api_data/utils/data_utils.dart';
import '../../controllers/update_profile_controller.dart';

class UpdateProfile extends StatefulWidget {
  final bool? isuUpdateScreen;

  const UpdateProfile({Key? key, this.isuUpdateScreen}) : super(key: key);

  @override
  State<UpdateProfile> createState() => _UpdateProfileState();
}

class _UpdateProfileState extends State<UpdateProfile> {



  final GlobalKey<FormState> _formKey = GlobalKey();
  final TextEditingController _firstNameCtrl =
      TextEditingController(text: AuthUtility.userInfo.data?.firstName ?? '');
  final TextEditingController _lastNameCtrl =
      TextEditingController(text: AuthUtility.userInfo.data?.lastName ?? '');
  final TextEditingController _mobileCtrl =
      TextEditingController(text: AuthUtility.userInfo.data?.mobile ?? '');
  final TextEditingController _passwordCtrl = TextEditingController();


  Future<void> pickImage() async {
    if (mounted) {
      await showDialog(
        context: context,
        builder: (BuildContext context) {
          return GetBuilder<UpdateProfileController>(
              builder: (updateProfileController){
                return AlertDialog(
                  title: const Text('Select Image Source'),
                  actions: [
                    TextButton(
                      onPressed: () {
                        updateProfileController.selectImage();
                        Get.back();
                      },
                      child: const Text('Gallery'),
                    ),
                    TextButton(
                      onPressed: () {
                        Get.back();
                        //todo
                      },
                      child: const Text('Camera'),
                    ),
                  ],
                );
              }
          );
        },
      );
    }
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const ProfileBannerWidget(
        isuUpdateScreen: true,
      ),
      body: ScreenBg(
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const SizedBox(height: 20),
              Form(
                key: _formKey,
                child: Padding(
                  padding: const EdgeInsets.all(25.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Update Profile',
                        style: Theme.of(context).textTheme.titleLarge,
                      ),
                      const SizedBox(height: 20),
                      InkWell(
                        onTap: () {
                          pickImage();
                        },
                        child: Container(
                          width: double.maxFinite,
                          decoration: const BoxDecoration(
                            color: Colors.white,
                          ),
                          child: Row(
                            children: [
                              Container(
                                padding:
                                    const EdgeInsets.symmetric(vertical: 16),
                                alignment: Alignment.center,
                                decoration: const BoxDecoration(
                                  color: Colors.green,
                                ),
                                width: 90,
                                child: const Text(
                                  'Photo',
                                  style: TextStyle(color: Colors.white),
                                ),
                              ),
                              const SizedBox(width: 15),
                              GetBuilder<UpdateProfileController>(builder: (updateProfileController){
                                return Visibility(
                                  visible: updateProfileController.imageFile != null,
                                  replacement: const Text('Upload Image'),
                                  child: SizedBox(
                                    width: 180,
                                    child: Text(
                                      updateProfileController.imageFile?.name ?? ' ',
                                      style: const TextStyle(
                                        overflow: TextOverflow.ellipsis,
                                        fontSize: 14,
                                        // Adjust the font size as needed
                                      ),
                                    ),
                                  ),
                                );
                              }),
                            ],
                          ),
                        ),
                      ),
                      const SizedBox(height: 10),
                      TextFormField(
                        readOnly: true,
                        initialValue: AuthUtility.userInfo.data?.email ?? '',
                        decoration:
                        const InputDecoration(hintText: 'Email'),
                      ),
                      const SizedBox(height: 10),
                      TextFormField(
                        controller: _firstNameCtrl,
                        decoration:
                            const InputDecoration(hintText: 'First Name'),
                      ),
                      const SizedBox(height: 10),
                      TextFormField(
                        controller: _lastNameCtrl,
                        decoration:
                            const InputDecoration(hintText: 'Last Name'),
                      ),
                      TextFormField(
                        controller: _mobileCtrl,
                        decoration: const InputDecoration(hintText: 'Mobile'),
                      ),
                      const SizedBox(height: 10),
                      GetBuilder<UpdateProfileController>(builder: (updateProfileController){
                        return TextFormField(
                          controller: _passwordCtrl,
                          obscureText: updateProfileController.obscureState,
                          decoration: InputDecoration(
                            hintText: 'Password',
                            suffixIcon: InkWell(
                              onTap: () {
                                updateProfileController.toggleObscureState();
                              },
                              child: Icon(!updateProfileController.obscureState
                                  ? Icons.visibility
                                  : Icons.visibility_off),
                            ),
                          ),
                          validator: (String? value) {
                            if (value?.isNotEmpty == true && value!.length < 6) {
                              return 'Password must have a minimum of 6 characters';
                            }
                            return null;
                          },
                        );
                      }),

                      const SizedBox(height: 10),
                      GetBuilder<UpdateProfileController>(builder: (updateProfileController){
                        return SizedBox(
                          width: double.maxFinite,
                          child: Visibility(
                            visible: !updateProfileController.isLoading,
                            replacement: const Center(child: CircularProgressIndicator(),),
                            child: ElevatedButton(
                              onPressed: () {
                                if (!_formKey.currentState!.validate()) {
                                  return;
                                }
                                updateProfileController.updateUserProfile(
                                    _firstNameCtrl.text.trim(),
                                    _lastNameCtrl.text.trim(),
                                    _mobileCtrl.text.trim(),
                                    _passwordCtrl.text,
                                ).then((result) {
                                  if(result){
                                    Get.snackbar('Profile updated', 'Profile updated');
                                    Get.offAllNamed('/bottomHome', arguments: updateProfileController.imageFile?.path);
                                  }else {
                                    Get.snackbar('Updated Failed', 'Profile update failed. Please try again.');
                                  }
                                });
                              },
                              child: const Icon(Icons.arrow_circle_right_outlined),
                            ),
                          ),
                        );
                      }),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
