import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:itask_complete_project/ui/widgets/screen_bg.dart';

import '../../controllers/rest_pass_controller.dart';


class ResetPasswordScreen extends StatefulWidget {
  const ResetPasswordScreen({Key? key}) : super(key: key);

  @override
  State<ResetPasswordScreen> createState() => _ResetPasswordScreenState();
}

class _ResetPasswordScreenState extends State<ResetPasswordScreen> {


  final TextEditingController _passwordCtrl = TextEditingController();
  final TextEditingController _confirmPasswordCtrl = TextEditingController();
  final GlobalKey<FormState> _formKey = GlobalKey();

  // bool obscureState = true,
  //     isLoading = false,
  //     confirmObscureState = true,
  //     passwordMatch = true;
  //
  // void checkPasswordMatch() {
  //   final String password = _passwordCtrl.text;
  //   final String confirmPassword = _confirmPasswordCtrl.text;
  //   setState(() {
  //     passwordMatch = password == confirmPassword;
  //   });
  // }
  //
  // Future<void> resetPasswordMethod(String email, String otp) async {
  //   isLoading = true;
  //   setState(() {});
  //
  //
  //   Map<String, dynamic> requestBody = {
  //     "email": email,
  //     "OTP": otp,
  //     "password": _confirmPasswordCtrl.text
  //   };
  //
  //   final NetworkResponse response =
  //       await NetworkServiceHandler().postRequest(Urls.resetPasswordUrl, requestBody);
  //
  //
  //   isLoading = false;
  //   if (mounted) {
  //     setState(() {});
  //   }
  //
  //   if (response.isSuccess) {
  //     if (mounted) {
  //       Get.offAllNamed('/login');
  //       ScaffoldMessenger.of(context).showSnackBar(
  //           const SnackBar(content: Text('Password reset Successfully')));
  //     }
  //   } else {
  //     if (mounted) {
  //       ScaffoldMessenger.of(context).showSnackBar(
  //           const SnackBar(content: Text('Password reset failed')));
  //     }
  //   }
  // }

  @override
  Widget build(BuildContext context) {
    final Map<String, dynamic>? optEmailArguments =
        ModalRoute.of(context)?.settings.arguments as Map<String, dynamic>?;

    return Scaffold(
      body: ScreenBg(
        child: Center(
          child: Padding(
            padding: const EdgeInsets.all(25.0),
            child: SingleChildScrollView(
              child: Form(
                key: _formKey,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'Set Password',
                      style: Theme.of(context).textTheme.titleLarge,
                    ),
                    const SizedBox(height: 5),
                    Text(
                      'Minimum length of password is 8 characters with letter and number combination',
                      style: Theme.of(context).textTheme.bodyMedium,
                    ),
                    const SizedBox(height: 15),
                    GetBuilder<ResetPassController>(
                        builder: (resetPassController) {
                      return TextFormField(
                        controller: _passwordCtrl,
                        obscureText: resetPassController.obscureState,
                        onChanged: (_) => resetPassController.passwordMatch,
                        decoration: InputDecoration(
                          hintText: 'Password',
                          errorText: resetPassController.passwordMatch ? null : "Passwords don't match",
                          suffixIcon: InkWell(
                            onTap: () {
                              resetPassController
                                  .toggleObscureState();
                            },
                            child: Icon(!resetPassController.obscureState
                                ? Icons.visibility
                                : Icons.visibility_off),
                          ),
                        ),
                        validator: (value) {
                          if (value!.isEmpty) {
                            return 'Password cannot be empty';
                          } else if (value.length < 6) {
                            return 'Password must have a minimum of 6 characters';
                          }
                          return null;
                        },
                      );
                    }),
                    const SizedBox(height: 15),
                    GetBuilder<ResetPassController>(
                        builder: (resetPassController){
                          return TextFormField(
                            controller: _confirmPasswordCtrl,
                            obscureText: resetPassController.obscureState,
                            onChanged: (_) => resetPassController.toggleConfirmObscureState(),
                            decoration: InputDecoration(
                              hintText: 'Confirm Password',
                              errorText: resetPassController.passwordMatch ? null : "Passwords don't match",
                              suffixIcon: InkWell(
                                onTap: () {
                                  resetPassController
                                      .toggleConfirmObscureState();
                                },
                                child: Icon(!resetPassController.confirmObscureState
                                    ? Icons.visibility
                                    : Icons.visibility_off),
                              ),
                            ),
                            validator: (value) {
                              if (value!.isEmpty) {
                                return 'Confirm Password cannot be empty';
                              } else if (value.length < 6) {
                                return 'Confirm Password must have a minimum of 6 characters';
                              }
                              return null;
                            },
                          );
                        }
                    ),
                    const SizedBox(height: 15),
                    GetBuilder<ResetPassController>(
                        builder: (resetPassController){
                      return  SizedBox(
                        width: double.infinity,
                        child: Visibility(
                          visible: !resetPassController.isLoading,
                          replacement: const Center(child: CircularProgressIndicator(),),
                          child: ElevatedButton(
                            onPressed: () {
                              if (!_formKey.currentState!.validate()) {
                                return;
                              }

                              if (optEmailArguments != null) {
                                String email = optEmailArguments['email'];
                                String otp = optEmailArguments['otp'];
                                resetPassController.resetPasswordMethod(email, otp, _passwordCtrl.text).then((result) => {
                                  if(result){
                                    Get.toNamed('/login'),
                                    Get.snackbar('Password Reset', 'Password reset done, Now login!'),
                                  }else {
                                    Get.snackbar('Invalid input', 'Invalid input'),
                                  }
                                });
                              }
                            },
                            child: const Text('Confirm'),
                          ),
                        ),
                      );
                    }),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text('Have an account?',
                            style: Theme.of(context).textTheme.bodyMedium),
                        TextButton(
                          onPressed: () {
                            Navigator.pushNamed(context, '/login');
                          },
                          child: const Text('Sign in'),
                        ),
                      ],
                    )
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
