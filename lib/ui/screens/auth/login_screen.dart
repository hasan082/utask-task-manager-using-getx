import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:itask_complete_project/ui/controllers/login_controller.dart';
import '../../widgets/screen_bg.dart';

class LoginScreen extends StatelessWidget {
  LoginScreen({Key? key}) : super(key: key);

  final TextEditingController _emailCtrl = TextEditingController();

  final TextEditingController _passwordCtrl = TextEditingController();

  final GlobalKey<FormState> _formKey = GlobalKey();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ScreenBg(
        child: Center(
          child: Padding(
            padding: const EdgeInsets.all(25.0),
            child: SingleChildScrollView(
              child: Form(
                key: _formKey,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'Get Started with',
                      style: Theme.of(context).textTheme.titleLarge,
                    ),
                    const SizedBox(height: 15),
                    TextFormField(
                      controller: _emailCtrl,
                      decoration: const InputDecoration(hintText: 'Email'),
                      validator: (String? value) {
                        final emailRegex =
                            RegExp(r'^[\w-.]+@([\w-]+\.)+[\w-]{2,4}$');
                        if (value?.isEmpty ?? true) {
                          return 'Enter your Email';
                        } else if (!emailRegex.hasMatch(value!)) {
                          return 'Enter a valid email address';
                        }
                        return null;
                      },
                    ),
                    const SizedBox(height: 15),
                    GetBuilder<LoginController>(
                      builder: (loginController) {
                        return TextFormField(
                          controller: _passwordCtrl,
                          obscureText: loginController.obscureState,
                          decoration: InputDecoration(
                            hintText: 'Password',
                            suffixIcon: InkWell(
                              onTap: () {
                                loginController
                                    .toggleObscureState();
                              },
                              child: Icon(!loginController.obscureState
                                  ? Icons.visibility
                                  : Icons.visibility_off),
                            ),
                            //
                          ),
                          validator: (String? value) {
                            if (value!.isEmpty || value.length < 6) {
                              return 'Password must have a minimum of 6 characters';
                            }
                            return null;
                          },
                        );
                      },
                    ),
                    const SizedBox(height: 15),
                    GetBuilder<LoginController>(
                      builder: (loginController) {
                        return SizedBox(
                          width: double.maxFinite,
                          child: Visibility(
                            visible: loginController.isLoading == false,
                            replacement: const Center(
                              child: CircularProgressIndicator(),
                            ),
                            child: ElevatedButton(
                              onPressed: () {
                                if (!_formKey.currentState!.validate()) {
                                  return;
                                }
                                loginController
                                    .userLogIn(_emailCtrl.text.trim(),
                                        _passwordCtrl.text)
                                    .then((result) {
                                  result == true
                                      ? Get.offAllNamed('/bottomHome')
                                      : Get.snackbar(
                                          "Failed", "Incorrect Credentials");
                                });
                              },
                              child: const Icon(Icons.arrow_forward_ios),
                            ),
                          ),
                        );
                      },
                    ),
                    const SizedBox(height: 40),
                    Center(
                      child: InkWell(
                        onTap: () {
                          Get.toNamed('/forgetPass');
                        },
                        child: const Text('Forgot Password?'),
                      ),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        const Center(child: Text('Don\'t have an account?')),
                        TextButton(
                          onPressed: () {
                            Get.toNamed('/signUp');
                          },
                          child: const Text('Sign up'),
                        ),
                      ],
                    )
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
