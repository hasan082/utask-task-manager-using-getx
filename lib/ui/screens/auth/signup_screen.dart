import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:itask_complete_project/ui/controllers/signup_controller.dart';
import '../../widgets/screen_bg.dart';

class SignUpScreen extends StatefulWidget {
  const SignUpScreen({Key? key}) : super(key: key);

  @override
  State<SignUpScreen> createState() => _SignUpScreenState();
}

class _SignUpScreenState extends State<SignUpScreen> {
  bool obscureState = true;
  final GlobalKey<FormState> _formKey = GlobalKey();
  final TextEditingController _emailCtrl = TextEditingController();
  final TextEditingController _firstNameCtrl = TextEditingController();
  final TextEditingController _lastNameCtrl = TextEditingController();
  final TextEditingController _mobileCtrl = TextEditingController();
  final TextEditingController _passwordCtrl = TextEditingController();

  // bool _isSignUpProgress = false;
  //
  // Future<void> userSignUp() async {
  //   _isSignUpProgress = true;
  //   if (mounted) {
  //     setState(() {});
  //   }
  //
  //   NetworkServiceHandler networkServiceHandler = NetworkServiceHandler();
  //
  //   Map<String, dynamic> requestBody = {
  //     "email": _emailCtrl.text.trim(),
  //     "firstName": _firstNameCtrl.text.trim(),
  //     "lastName": _lastNameCtrl.text.trim(),
  //     "mobile": _mobileCtrl.text.trim(),
  //     "password": _passwordCtrl.text,
  //     "photo": "",
  //   };
  //
  //   // Check if the email already exist in the backend
  //   final emailExists = await checkEmailExists(_emailCtrl.text.trim());
  //
  //   if (emailExists) {
  //     if (mounted) {
  //       _isSignUpProgress = false;
  //       if (mounted) {
  //         setState(() {});
  //       }
  //       ScaffoldMessenger.of(context).showSnackBar(
  //         const SnackBar(
  //           content: Text('Email already exists. Try different email.'),
  //         ),
  //       );
  //     }
  //     _isSignUpProgress = false;
  //   } else {
  //     final response = await networkServiceHandler.postRequest(
  //       Urls.registrationUrls,
  //       requestBody,
  //     );
  //     if (mounted) {
  //       if (response.isSuccess) {
  //         ScaffoldMessenger.of(context).showSnackBar(
  //           const SnackBar(
  //               content: Text('Registration Successful! Please Login')),
  //         );
  //         if (mounted) {
  //           setState(() {});
  //           _isSignUpProgress = false;
  //         }
  //         _emailCtrl.clear();
  //         _firstNameCtrl.clear();
  //         _lastNameCtrl.clear();
  //         _mobileCtrl.clear();
  //         _passwordCtrl.clear();
  //
  //         Get.toNamed('/login');
  //       } else {
  //         ScaffoldMessenger.of(context).showSnackBar(
  //           const SnackBar(content: Text('Registration Failed')),
  //         );
  //       }
  //     }
  //   }
  // }
  //
  // Future<bool> checkEmailExists(String email) async {
  //   try {
  //
  //     NetworkServiceHandler networkServiceHandler = NetworkServiceHandler();
  //
  //     Map<String, dynamic> requestBody = {
  //       "email": _emailCtrl.text.trim(),
  //     };
  //     final response = await networkServiceHandler.postRequest(
  //       Urls.loginUrls,
  //       requestBody,
  //     );
  //     if (response.statusCode == 200) {
  //       return true;
  //     } else {
  //       return false;
  //     }
  //   } catch (e) {
  //     return false;
  //   }
  // }
  //

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ScreenBg(
        child: Center(
          child: Padding(
            padding: const EdgeInsets.all(25.0),
            child: SingleChildScrollView(
              child: Form(
                key: _formKey,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'Join With Us',
                      style: Theme.of(context).textTheme.titleLarge,
                    ),
                    const SizedBox(height: 15),
                    TextFormField(
                      controller: _emailCtrl,
                      keyboardType: TextInputType.emailAddress,
                      decoration: const InputDecoration(hintText: 'Email'),
                      validator: (String? value) {
                        final emailRegex =
                            RegExp(r'^[\w-.]+@([\w-]+\.)+[\w-]{2,4}$');
                        if (value?.isEmpty ?? true) {
                          return 'Enter your Email';
                        } else if (!emailRegex.hasMatch(value!)) {
                          return 'Enter a valid email address';
                        }
                        return null;
                      },
                    ),
                    const SizedBox(height: 10),
                    TextFormField(
                      controller: _firstNameCtrl,
                      decoration: const InputDecoration(hintText: 'First Name'),
                      validator: (String? value) {
                        if (value?.isEmpty ?? true) {
                          return 'Enter your First Name';
                        }
                        return null;
                      },
                    ),
                    const SizedBox(height: 10),
                    TextFormField(
                      controller: _lastNameCtrl,
                      decoration: const InputDecoration(hintText: 'Last Name'),
                      validator: (String? value) {
                        if (value?.isEmpty ?? true) {
                          return 'Enter your Last Name';
                        }
                        return null;
                      },
                    ),
                    const SizedBox(height: 10),
                    TextFormField(
                      controller: _mobileCtrl,
                      keyboardType: TextInputType.number,
                      decoration: const InputDecoration(hintText: 'Mobile'),
                      validator: (String? value) {
                        if (value == null || value.isEmpty) {
                          return 'Enter your mobile number';
                        }
                        RegExp regex = RegExp(r'^[0-9]+$');
                        if (!regex.hasMatch(value)) {
                          return 'Mobile should contains only digits';
                        }

                        if (value.length != 11) {
                          return 'Mobile should be 11 digits';
                        }

                        return null;
                      },
                    ),
                    const SizedBox(height: 10),
                    GetBuilder<SignUpController>(
                      builder: (signupController) {
                        return TextFormField(
                          controller: _passwordCtrl,
                          obscureText: signupController.obscureState,
                          decoration: InputDecoration(
                            hintText: 'Password',
                            suffixIcon: InkWell(
                              onTap: () {
                                signupController.toggleObscureState();
                              },
                              child: Icon(!signupController.obscureState
                                  ? Icons.visibility
                                  : Icons.visibility_off),
                            ),
                            //
                          ),
                          validator: (String? value) {
                            if (value!.isEmpty || value.length < 6) {
                              return 'Password must have a minimum of 6 characters';
                            }
                            return null;
                          },
                        );
                      },
                    ),
                    const SizedBox(height: 15),
                    GetBuilder<SignUpController>(builder: (signupController) {
                      return SizedBox(
                        width: double.maxFinite,
                        child: Visibility(
                          visible: signupController.isLoading == false,
                          replacement:
                              const Center(child: CircularProgressIndicator()),
                          child: ElevatedButton(
                            onPressed: () {
                              if (!_formKey.currentState!.validate()) {
                                return;
                              }

                              signupController
                                  .checkEmailExists(_emailCtrl.text.trim())
                                  .then((emailExist) {
                                if (emailExist) {
                                  Get.snackbar("Email exists",
                                      "Email already registered. Try different email.");
                                } else {
                                  signupController
                                      .userSignUp(
                                          _emailCtrl.text.trim(),
                                          _firstNameCtrl.text.trim(),
                                          _lastNameCtrl.text.trim(),
                                          _mobileCtrl.text.trim(),
                                          _passwordCtrl.text)
                                      .then((result) {
                                    if (result) {
                                      Get.snackbar("Success!",
                                          "Registration Successful! Please Login'");
                                      _emailCtrl.clear();
                                      _firstNameCtrl.clear();
                                      _lastNameCtrl.clear();
                                      _mobileCtrl.clear();
                                      _passwordCtrl.clear();
                                      Get.toNamed('/login');
                                    } else {
                                      Get.snackbar(
                                          "Failed!", "Registration Failed");
                                    }
                                  });
                                }
                              });
                            },
                            child:
                                const Icon(Icons.arrow_circle_right_outlined),
                          ),
                        ),
                      );
                    }),
                    const SizedBox(height: 5),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        const Center(child: Text('Have an account?')),
                        TextButton(
                          onPressed: () {
                            Get.toNamed('/login');
                          },
                          child: const Text('Sign in'),
                        ),
                      ],
                    )
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
