import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:itask_complete_project/ui/controllers/forgot_password_controller.dart';
import 'package:itask_complete_project/ui/widgets/screen_bg.dart';

class ForgetPassword extends StatefulWidget {
  const ForgetPassword({Key? key}) : super(key: key);

  @override
  State<ForgetPassword> createState() => _ForgetPasswordState();
}

class _ForgetPasswordState extends State<ForgetPassword> {
  final GlobalKey<FormState> _formKey = GlobalKey();
  final TextEditingController _emailCtrl = TextEditingController();


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ScreenBg(
        child: Center(
          child: Padding(
            padding: const EdgeInsets.all(25.0),
            child: SingleChildScrollView(
              child: Form(
                key: _formKey,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'Your Email Address',
                      style: Theme.of(context).textTheme.titleLarge,
                    ),
                    const SizedBox(height: 5),
                    Text(
                      'A 6 digit verification code sent to your email address',
                      style: Theme.of(context).textTheme.bodyMedium,
                    ),
                    const SizedBox(height: 15),
                    TextFormField(
                      controller: _emailCtrl,
                      decoration: const InputDecoration(
                        hintText: 'Email',
                      ),
                      keyboardType: TextInputType.emailAddress,
                      validator: (String? value) {
                        final emailRegex =
                            RegExp(r'^[\w-.]+@([\w-]+\.)+[\w-]{2,4}$');
                        if (value?.isEmpty ?? true) {
                          return 'Enter your Email';
                        } else if (!emailRegex.hasMatch(value!)) {
                          return 'Enter a valid email address';
                        }
                        return null;
                      },
                    ),
                    const SizedBox(height: 15),
                    GetBuilder<ForgetPassController>(
                        builder: (forgetController) {
                      return SizedBox(
                        width: double.infinity,
                        child: Visibility(
                          visible: !forgetController.isLoading,
                          replacement: const Center(
                            child: CircularProgressIndicator(),
                          ),
                          child: Visibility(
                            visible: !forgetController.isLoading,
                            replacement: const Center(
                              child: CircularProgressIndicator(),
                            ),
                            child: ElevatedButton(
                              onPressed: () {
                                if (!_formKey.currentState!.validate()) {
                                  return;
                                }

                                forgetController
                                    .forgetPassword(_emailCtrl.text.trim())
                                    .then((result) {
                                  if (result) {
                                    Get.toNamed('/otp',
                                        arguments: _emailCtrl.text.trim());
                                    Get.snackbar(
                                        'OTP Send', 'Check your email for otp');
                                  } else {
                                    Get.snackbar('Email not registered',
                                        'Double Check your email is correct or put registered email');
                                  }
                                });
                              },
                              child: const Icon(Icons.arrow_forward_ios),
                            ),
                          ),
                        ),
                      );
                    }),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text('Have an account?',
                            style: Theme.of(context).textTheme.bodyMedium),
                        TextButton(
                          onPressed: () {
                            Get.toNamed('/login');
                          },
                          child: const Text('Sign in'),
                        ),
                      ],
                    )
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
