

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:itask_complete_project/ui/controllers/otp_controller.dart';
import 'package:itask_complete_project/ui/widgets/screen_bg.dart';
import 'package:pin_code_fields/pin_code_fields.dart';


class OtpVerificationScreen extends StatefulWidget {

  const OtpVerificationScreen({Key? key}) : super(key: key);

  @override
  State<OtpVerificationScreen> createState() => _OtpVerificationScreenState();
}

class _OtpVerificationScreenState extends State<OtpVerificationScreen> {
  final TextEditingController _otpCtrl = TextEditingController();
  final GlobalKey<FormState> _formKey = GlobalKey();





  @override
  Widget build(BuildContext context) {
    final emailArguments = ModalRoute.of(context)?.settings.arguments as String;
    return Scaffold(
      body: ScreenBg(
        child: Center(
          child: Padding(
            padding: const EdgeInsets.all(25.0),
            child: SingleChildScrollView(
              child: Form(
                key: _formKey,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'Pin Verification',
                      style: Theme.of(context).textTheme.titleLarge,
                    ),
                    const SizedBox(height: 5),
                    Text(
                      '6 digit verification code send to your email',
                      style: Theme.of(context).textTheme.bodyMedium,
                    ),
                    const SizedBox(height: 15),
                    PinCodeTextField(
                      controller: _otpCtrl,
                      length: 6,
                      obscureText: false,
                      animationType: AnimationType.fade,
                      pinTheme: PinTheme(
                          shape: PinCodeFieldShape.box,
                          borderRadius: BorderRadius.circular(5),
                          fieldHeight: 50,
                          fieldWidth: 40,
                          activeFillColor: Colors.white,
                          selectedColor: Colors.white,
                          selectedFillColor: Colors.green.shade500,
                          inactiveFillColor: Colors.white54),
                      animationDuration: const Duration(milliseconds: 300),
                      backgroundColor: Colors.blue.shade50,
                      enableActiveFill: true,
                      onCompleted: (v) {},
                      beforeTextPaste: (text) {
                        return true;
                      },
                      appContext: context,
                    ),
                    const SizedBox(height: 15),
                    GetBuilder<OtpController>(
                        builder: (otpController){
                      return SizedBox(
                        width: double.infinity,
                        child: Visibility(
                          visible: !otpController.isLoading,
                          replacement: const Center(child: CircularProgressIndicator(),),
                          child: ElevatedButton(
                            onPressed: () {
                              if (!_formKey.currentState!.validate()) {
                                return;
                              }
                              otpController.otpVerifyMethod(emailArguments, _otpCtrl.text).then((result) {
                                if(result){
                                  Get.offAllNamed('/resetPass', arguments: {
                                    'email': emailArguments,
                                    'otp': _otpCtrl.text,
                                  });
                                  Get.snackbar('OTP Verified', 'OTP Verified Successfully');
                                }else {
                                  Get.snackbar('Wrong OTP', 'Double check your otp');
                                }
                              });
                            },
                            child: const Text('Verify'),
                          ),
                        ),
                      );
                    }),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text('Have an account?',
                            style: Theme.of(context).textTheme.bodyMedium),
                        TextButton(
                          onPressed: () {
                            Get.toNamed('/login');
                          },
                          child: const Text('Sign in'),
                        ),
                      ],
                    )
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}


