import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:itask_complete_project/ui/controllers/addtask_controller.dart';
import 'package:itask_complete_project/ui/widgets/screen_bg.dart';
import '../widgets/profile_banner_widget.dart';


class AddTaskScreen extends StatefulWidget {
  const AddTaskScreen({Key? key}) : super(key: key);

  @override
  State<AddTaskScreen> createState() => _AddTaskScreenState();
}

class _AddTaskScreenState extends State<AddTaskScreen> {
  final GlobalKey<FormState> _formKey = GlobalKey();
  final TextEditingController _titleCtrl = TextEditingController();
  final TextEditingController _descriptionCtrl = TextEditingController();


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const ProfileBannerWidget(),
      body: ScreenBg(
        child: SingleChildScrollView(
          child: Form(
            key: _formKey,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const SizedBox(
                  height: 20,
                ),
                Padding(
                  padding: const EdgeInsets.all(25.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Add New Task',
                        style: Theme.of(context).textTheme.titleLarge,
                      ),
                      const SizedBox(height: 15),
                      TextFormField(
                        controller: _titleCtrl,
                        decoration: const InputDecoration(hintText: 'Title'),
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return 'Please enter a title.';
                          }
                          return null; // Return null if the value is valid
                        },
                      ),
                      const SizedBox(height: 15),
                      TextFormField(
                        controller: _descriptionCtrl,
                        maxLines: 5,
                        decoration: const InputDecoration(
                          hintText: 'Description',
                        ),
                      ),
                      const SizedBox(height: 15),
                      GetBuilder<AddTaskController>(builder: (addTaskController){
                        return SizedBox(
                          width: double.infinity,
                          child: Visibility(
                            visible: !addTaskController.isLoading,
                            replacement:
                            const Center(child: CircularProgressIndicator()),
                            child: ElevatedButton(
                              onPressed: () {
                                if (_formKey.currentState!.validate()) {
                                  addTaskController.addNewTaskMethod(
                                      _titleCtrl.text.trim(),
                                      _descriptionCtrl.text.trim(),
                                  ).then((result) {
                                    if(result){
                                      _titleCtrl.clear();
                                      _descriptionCtrl.clear();
                                      Get.offAllNamed('/bottomHome');
                                      Get.snackbar('Task Added!', 'Task Added Successfully');
                                    }else {
                                      Get.snackbar('Task add Failed!', 'Task Added Failed! Try Again!');
                                    }
                                  });
                                }
                              },
                              child: const Icon(Icons.arrow_forward_ios),
                            ),
                          ),
                        );
                      }),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
