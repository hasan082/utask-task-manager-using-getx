import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:itask_complete_project/utils/assets_utils.dart';
import '../controllers/splash_screen_sontroller.dart';
import '../widgets/screen_bg.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen>
    with SingleTickerProviderStateMixin {
  final SplashController splashController = Get.find<SplashController>();

  @override
  void initState() {
    super.initState();
    initializeController();
  }

  void initializeController() async {

    bool isLoggedIn = await splashController.animationControllerMethod();

    if (isLoggedIn) {
      if (mounted) {
        Navigator.pushReplacementNamed(context, '/bottomHome');
      }
    } else {
      if (mounted) {
        Navigator.pushReplacementNamed(context, '/login');
      }
    }
  }

  @override
  void dispose() {
    splashController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ScreenBg(
        child: Center(
          child: Column(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Image.asset(
                AssetUtils.logoJpgWhite,
                width: 90,
              ),
              const SizedBox(
                height: 16,
              ),
              GetBuilder<SplashController>(
                  builder: (splashController) {
                return SizedBox(
                  width: 120,
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(10),
                    child: AnimatedBuilder(
                      animation: splashController.controller,
                      builder: (BuildContext context, Widget? child) {
                        return LinearProgressIndicator(
                          value: splashController.controller.value.toDouble(),
                          minHeight: 4,
                          backgroundColor: const Color(0xFFE8D167),
                          valueColor: const AlwaysStoppedAnimation<Color>(
                            Color(0xFF9E5D1B),
                          ),
                        );
                      },
                    ),
                  ),
                );
              }),
            ],
          ),
        ),
      ),
    );
  }
}
