import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:itask_complete_project/api_data/api_services/network_service_handler.dart';
import 'package:itask_complete_project/api_data/models/network_services_response.dart';
import 'package:itask_complete_project/api_data/utils/data_utils.dart';
import 'package:itask_complete_project/ui/widgets/update_task_status_widget.dart';
import '../../api_data/models/task_list_model.dart';
import '../controllers/new_task_list_data_controller.dart';
import '../controllers/overview_controller.dart';
import '../widgets/task_overview_widget.dart';
import '../widgets/task_single_item_widget.dart';
import '../widgets/edit_task_widget.dart';

class TaskNew extends StatefulWidget {
  const TaskNew({Key? key}) : super(key: key);

  @override
  State<TaskNew> createState() => _TaskNewState();
}

class _TaskNewState extends State<TaskNew> {
  late bool updateTaskInProgress = false;

  OverviewController overviewController = Get.find<OverviewController>();
  GetNewTaskListDataController getNewTaskListDataController =
      Get.find<GetNewTaskListDataController>();

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) {
      overviewController.getTaskOverviewData();
      getNewTaskListDataController.getNewTaskListData();
    });
  }

  Future<void> deleteTask(String taskId) async {
    final NetworkResponse response = await NetworkServiceHandler()
        .getRequest(Urls.deleteTaskStatusUrl(taskId));
    if (response.isSuccess) {
      getNewTaskListDataController.taskListModel.data!
          .removeWhere((element) => element.sId == taskId);
      if (mounted) {
        setState(() {});
        ScaffoldMessenger.of(context).showSnackBar(
            const SnackBar(content: Text('Task deleted successfully')));
      }
    } else {
      if (mounted) {
        ScaffoldMessenger.of(context)
            .showSnackBar(const SnackBar(content: Text('Deletion failed')));
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Get.toNamed('/addTask');
        },
        child: const Icon(Icons.add),
      ),
      body: SafeArea(
        child: Column(
          children: [
            GetBuilder<OverviewController>(builder: (_) {
              if (overviewController.isTaskOverViewPageProgress) {
                return const Center(
                  child: LinearProgressIndicator(),
                );
              } else {
                return Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Row(
                    children: [
                      TaskOverview(
                        taskNumber: getTaskNumberForStatus("New"),
                        overviewStatus: "New",
                      ),
                      TaskOverview(
                        taskNumber: getTaskNumberForStatus("Progress"),
                        overviewStatus: "Progress",
                      ),
                      TaskOverview(
                        taskNumber: getTaskNumberForStatus("Completed"),
                        overviewStatus: "Completed",
                      ),
                      TaskOverview(
                        taskNumber: getTaskNumberForStatus("Cancelled"),
                        overviewStatus: "Cancelled",
                      ),
                    ],
                  ),
                );
              }
            }),
            Expanded(
              child: GetBuilder<GetNewTaskListDataController>(
                builder: (_) {
                  if(getNewTaskListDataController.isNewTaskListInProgress){
                    return const Center(child: CircularProgressIndicator());
                  } else {
                    return Padding(
                      padding: const EdgeInsets.symmetric(
                          vertical: 10.0, horizontal: 6),
                      child: RefreshIndicator(
                        onRefresh: () async {
                          getNewTaskListDataController.getNewTaskListData();
                        },
                        child: ListView.separated(
                          shrinkWrap: true,
                          physics: const BouncingScrollPhysics(),
                          itemCount: getNewTaskListDataController
                              .taskListModel.data?.length ??
                              0,
                          itemBuilder: (context, index) {
                            return TaskSingleItemWidget(
                              chipBgColor: const Color(0xFF42C1E8),
                              taskData: getNewTaskListDataController
                                  .taskListModel.data![index],
                              onTapDelete: () {
                                deleteTask(getNewTaskListDataController
                                    .taskListModel.data![index].sId!);
                              },
                              onTapStatusChange: () {
                                statusUpdateBottomSheet(
                                    getNewTaskListDataController
                                        .taskListModel.data![index]);
                              },
                            );
                          },
                          separatorBuilder:
                              (BuildContext context, int index) {
                            return const SizedBox(
                              height: 10,
                            );
                          },
                        ),
                      ),
                    );
                  }
                },
              ),
            )
          ],
        ),
      ),
    );
  }

  void editTaskBottomSheet(TaskData task) {
    showModalBottomSheet(
        isScrollControlled: true,
        context: context,
        builder: (context) {
          return EditTaskBottomSheet(
            task: task,
            onUpdateStatus: () {
              getNewTaskListDataController.getNewTaskListData();
            },
          );
        });
  }

  void statusUpdateBottomSheet(TaskData task) {
    showModalBottomSheet(
        isScrollControlled: true,
        context: context,
        builder: (context) {
          return StatefulBuilder(
            builder: (context, updateState) {
              return UpdateTaskStatus(
                  task: task,
                  onUpdate: () {
                    getNewTaskListDataController.getNewTaskListData();
                    overviewController.getTaskOverviewData();
                  });
            },
          );
        });
  }

  int getTaskNumberForStatus(String status) {
    if (overviewController.taskOverviewModel.data == null) {
      return 0;
    }

    for (var task in overviewController.taskOverviewModel.data!) {
      if (task.sId == status) {
        return task.sum ?? 0;
      }
    }

    return 0;
  }
}
