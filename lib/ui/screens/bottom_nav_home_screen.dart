import 'package:flutter/material.dart';
import 'package:itask_complete_project/ui/screens/task_cancelled.dart';
import 'package:itask_complete_project/ui/screens/task_completed.dart';
import 'package:itask_complete_project/ui/screens/task_new.dart';
import 'package:itask_complete_project/ui/screens/task_progress.dart';
import '../widgets/profile_banner_widget.dart';


class BottomNavHomeScreen extends StatefulWidget {
  const BottomNavHomeScreen({Key? key}) : super(key: key);

  @override
  State<BottomNavHomeScreen> createState() => _BottomNavHomeScreenState();
}

class _BottomNavHomeScreenState extends State<BottomNavHomeScreen> {
  int _currentIndex = 0;
  final PageController _pageController = PageController();


  final List<Widget> _pages = [
    const TaskNew(),
    const TaskProgress(),
    const TaskCompleted(),
    const TaskCancelled(),
  ];

  void _onItemTapped(int index) {
    if (_currentIndex != index) {
      setState(() {
        _currentIndex = index;
      });
      _pageController.animateToPage(
        index,
        duration: const Duration(milliseconds: 300),
        curve: Curves.easeInOut,
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xFFFAF8F6),
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        currentIndex: _currentIndex,
        backgroundColor: Colors.white,
        selectedItemColor: Colors.green,
        unselectedItemColor: Colors.grey,
         onTap:  _onItemTapped,
        items: const [
          BottomNavigationBarItem(
            label: 'New',
            icon: Icon(Icons.add_task_sharp),
          ),
          BottomNavigationBarItem(
            label: 'Progress',
            icon: Icon(Icons.run_circle_sharp),
          ),
          BottomNavigationBarItem(
            label: 'Completed',
            icon: Icon(Icons.done_all_sharp),
          ),
          BottomNavigationBarItem(
            label: 'Cancelled',
            icon: Icon(Icons.cancel_rounded),
          ),
        ],
      ),
      appBar:  const ProfileBannerWidget(),
      body: PageView(
    controller: _pageController,
    children: _pages,
    onPageChanged: (index){
      _currentIndex= index;
    }),
    );
  }
}

// I will need to work on it===
// PageView(
// controller: _pageController,
// children: _pages,
// onPageChanged: (index) {
// setState(() {
// _currentIndex = index;
// });
// },
// ),

// IndexedStack(
// index: _currentIndex,
// children: _pages,
// ),
// );