import 'dart:convert';
import 'dart:developer';
import 'package:itask_complete_project/api_data/models/login_response_model.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AuthUtility {
  AuthUtility._();

  static LoginResponseModel userInfo = LoginResponseModel();

  static Future<void> saveUserInfo(LoginResponseModel loginResponseModel) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    await sharedPreferences.setString('userinfo', jsonEncode(loginResponseModel.toJson()));
    userInfo = loginResponseModel;
  }

  static Future<LoginResponseModel?> getUserInfo() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    String? prefVal = sharedPreferences.getString('userinfo');
    if (prefVal == null) {
      return null;
    } else {
      LoginResponseModel loginResponseModel =
      LoginResponseModel.fromJson(jsonDecode(prefVal));
      return loginResponseModel;
    }
  }

  static Future<void> clearUserInfo() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    await sharedPreferences.clear();
  }

  static Future<bool> checkUserLoggedIn() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    bool isLoggedIn = sharedPreferences.containsKey('userinfo');
    log(isLoggedIn.toString());
    if (isLoggedIn) {
      userInfo = (await getUserInfo()) ?? LoginResponseModel();
    }
    return isLoggedIn;
  }

}