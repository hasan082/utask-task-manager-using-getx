class Urls {
  Urls._();
  static const String _baseUrl = "https://task.teamrabbil.com/api/v1";

  static const String registrationUrls = '$_baseUrl/registration';
  static const String loginUrls = '$_baseUrl/login';
  static const String createNewTaskUrls = '$_baseUrl/createTask';
  static const String profileUpdateUrls = '$_baseUrl/profileUpdate';
  static const String taskOverUrl = '$_baseUrl/taskStatusCount';
  static const String newTaskDataUrl = '$_baseUrl/listTaskByStatus/New';
  static const String progressTaskDataUrl = '$_baseUrl/listTaskByStatus/Progress';
  static const String completedTaskDataUrl = '$_baseUrl/listTaskByStatus/Completed';
  static const String cancelledTaskDataUrl = '$_baseUrl/listTaskByStatus/Cancelled';
  static const String resetPasswordUrl = '$_baseUrl/RecoverResetPass';

  static String updateTaskStatusUrl(String id, String status) => '$_baseUrl/updateTaskStatus/$id/$status';
  static String deleteTaskStatusUrl(String id) => '$_baseUrl/deleteTask/$id';
  static String forGetPasswordUrl(String email) => '$_baseUrl/RecoverVerifyEmail/$email';
  static String otpVerifyUrl(String email, String otp) => '$_baseUrl/RecoverVerifyOTP/$email/$otp';



}