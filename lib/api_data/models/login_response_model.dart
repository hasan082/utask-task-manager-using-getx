class LoginResponseModel {
  String? status;
  String? token;
  Data? _data;

  LoginResponseModel({String? status, String? token, Data? data}) {
    if (status != null) {
      status = status;
    }
    if (token != null) {
      token = token;
    }
    if (data != null) {
      _data = data;
    }
  }
  Data? get data => _data;
  set data(Data? data) => _data = data;

  LoginResponseModel.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    token = json['token'];
    _data = json['data'] != null ? Data.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['status'] = status;
    data['token'] = token;
    if (_data != null) {
      data['data'] = _data!.toJson();
    }
    return data;
  }
}

class Data {
  String? _email;
  String? _firstName;
  String? _lastName;
  String? _mobile;
  String? _photo;

  Data(
      {String? email,
        String? firstName,
        String? lastName,
        String? mobile,
        String? photo}) {
    if (email != null) {
      _email = email;
    }
    if (firstName != null) {
      _firstName = firstName;
    }
    if (lastName != null) {
      _lastName = lastName;
    }
    if (mobile != null) {
      _mobile = mobile;
    }
    if (photo != null) {
      _photo = photo;
    }
  }

  String? get email => _email;
  set email(String? email) => _email = email;
  String? get firstName => _firstName;
  set firstName(String? firstName) => _firstName = firstName;
  String? get lastName => _lastName;
  set lastName(String? lastName) => _lastName = lastName;
  String? get mobile => _mobile;
  set mobile(String? mobile) => _mobile = mobile;
  String? get photo => _photo;
  set photo(String? photo) => _photo = photo;

  Data.fromJson(Map<String, dynamic> json) {
    _email = json['email'];
    _firstName = json['firstName'];
    _lastName = json['lastName'];
    _mobile = json['mobile'];
    _photo = json['photo'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['email'] = _email;
    data['firstName'] = _firstName;
    data['lastName'] = _lastName;
    data['mobile'] = _mobile;
    data['photo'] = _photo;
    return data;
  }
}
