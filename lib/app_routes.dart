import 'package:get/get.dart';
import 'package:itask_complete_project/ui/screens/add_task_screen.dart';
import 'package:itask_complete_project/ui/screens/auth/otp_verification_screen.dart';
import 'package:itask_complete_project/ui/screens/auth/reset_password_screen.dart';
import 'package:itask_complete_project/ui/screens/auth/signup_screen.dart';
import 'package:itask_complete_project/ui/screens/auth/update_profile.dart';
import 'package:itask_complete_project/ui/screens/bottom_nav_home_screen.dart';
import 'package:itask_complete_project/ui/screens/auth/login_screen.dart';
import 'package:itask_complete_project/ui/screens/splash_screen.dart';
import 'package:itask_complete_project/ui/screens/auth/forget_password.dart';
import 'package:itask_complete_project/ui/screens/task_new.dart';

List<GetPage> allPageRoutes = [
  GetPage(name: '/splash', page: () => const SplashScreen()),
  GetPage(name: '/login', page: () => LoginScreen()),
  GetPage(name: '/resetPass', page: () => const ResetPasswordScreen()),
  GetPage(name: '/otp', page: () => const OtpVerificationScreen()),
  GetPage(name: '/forgetPass', page: () => const ForgetPassword()),
  GetPage(name: '/signUp', page: () => const SignUpScreen()),
  GetPage(name: '/bottomHome', page: () => const BottomNavHomeScreen()),
  GetPage(name: '/addTask', page: () => const AddTaskScreen()),
  GetPage(name: '/updateProfile', page: () => const UpdateProfile()),
  GetPage(name: '/newTaskAdd', page: () => const TaskNew()),
];
