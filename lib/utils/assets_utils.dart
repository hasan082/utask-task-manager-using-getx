class AssetUtils {
  AssetUtils._();
  static const String _imagePath = 'assets/images';

  static const String bgSvg = '$_imagePath/bg.svg';
  static const String logoJpgWhite = '$_imagePath/logo_white.jpg';
  static const String logoJpgBlack = '$_imagePath/logo_Black.jpg';
}